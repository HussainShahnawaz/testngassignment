

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

@Listeners(FailureTestListener.class)
public class LoginPage {


        public WebDriver driver;

        @DataProvider(name="Credentials")
        public static Object[][] getData() {
            return new Object[][]{
//                 {"shahnawaz@gmail.com", "1234343"},
                    {"hussainnn@gmail.com", "123456789@Xyz12"},
            };
        }

        @BeforeMethod
        public void setup(ITestContext context){
            WebDriverManager.chromedriver().setup();
            driver=new ChromeDriver();
            driver.get("http://practice.automationtesting.in/");
            context.setAttribute("driver",driver);
        }

        @Test(dataProvider = "Credentials")
        public void testLogin(String mail,String passwd){
            WebElement myAccount=driver.findElement(By.linkText("My Account"));
            myAccount.click();
            driver.findElement(By.id("username")).sendKeys(mail);
            driver.findElement(By.id("password")).sendKeys(passwd);
            driver.findElement(By.name("login")).click();
            String url=driver.getCurrentUrl();
            Assert.assertEquals(url,"http://practice.automationtesting.in/my-account/");
        }

    @Test(dataProvider = "Credentials")
    public void testLogin2(String mail,String passwd) {
        WebElement myAccount = driver.findElement(By.linkText("My Account"));
        myAccount.click();
        driver.findElement(By.id("username")).sendKeys(mail);
        driver.findElement(By.id("password")).sendKeys(passwd);
        driver.findElement(By.name("loginn")).click();
        String url = driver.getCurrentUrl();
        Assert.assertEquals(url, "http://practice.automationtesting.in/my-account/");
    }


        @AfterMethod
        public void exitbrowser(){
            driver.quit();
        }
    }



