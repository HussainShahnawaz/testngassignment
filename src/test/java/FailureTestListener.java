import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.annotations.Listeners;

import java.io.File;
import java.io.IOException;





public class FailureTestListener implements ITestListener{


    @Override
    public void onTestStart(ITestResult iTestResult) {

    }

    @Override
        public void onTestSuccess(ITestResult result) {
            System.out.println("The passed test case is:"+result.getName());
        }

        @Override
        public void onTestFailure(ITestResult result) {
            ITestContext context = result.getTestContext();
            WebDriver driver = (WebDriver)context.getAttribute("driver");

            File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            try {
                FileUtils.copyFile(screenshot, new File("FailedTest.png"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    @Override
    public void onTestSkipped(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {

    }
}

